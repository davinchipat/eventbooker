const mongoose = require('mongoose')

const Schema = mongoose.Schema

const EventSchema = new Schema({
    title: String,
    description: String ,
    price: Number,
    date: String,
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    }
}, {
    timestamps:true
})

const Event = mongoose.model('event', EventSchema)

module.exports = Event