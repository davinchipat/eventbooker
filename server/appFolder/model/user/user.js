const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
    username: String,
    password: String,
    email: String,
    eventsCreated: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "event"
        }
    ]
}, {
    timestamps:true
})

const User = mongoose.model('user', userSchema)

module.exports = User