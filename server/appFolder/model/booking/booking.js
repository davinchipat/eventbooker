const mongoose = require('mongoose')

const Schema = mongoose.Schema

const bookingSchema = new Schema({
    events: {
        type: Schema.Types.ObjectId,
        ref: 'event'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, 
{
    timestamps: true
})

const Booking = mongoose.model('booking', bookingSchema)

module.exports = Booking