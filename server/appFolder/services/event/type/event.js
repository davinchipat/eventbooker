const {gql} = require('apollo-server-express')

module.exports = gql`
    extend type Query{
        getEvents: [eventData]
        getSingleEvent(id:ID): eventData
        getUserEvent(userId:ID): [eventData]
    }

    extend type Mutation{
        createEvent(userId: ID! data: eventInput): String!
        updateEvent(id:ID! data:eventInput): String!
        deleteEvent(id:ID): String!
        deleteAllEvent:String! 
    }

    input eventInput {
        title: String
        description: String 
        price: Float
        date: String
    }

    type eventData {
        _id: ID
        title: String
        description: String 
        price: Float
        date: String
        creator: userData
    }
`