const EventMutation = {
  createEvent: async (_, { userId, data }, { datasources, req }, info) => {
    if (!req.isAuth) {
      throw new Error("Unauthorized");
    }
    const { EventAccount } = datasources;
    const response = await new EventAccount().createEvent(userId, data);
    console.log("sdsd");
    return response;
  },

  updateEvent: async (_, { id, data }, { datasources }, info) => {
    const { EventAccount } = datasources;
    const response = await new EventAccount().updateEvent(id, data);
    return response;
  },

  deleteEvent: async (_, { id }, { datasources }, info) => {
    const { EventAccount } = datasources;
    const response = await new EventAccount().deleteEvent(id);
    return "deleted";
  },

  deleteAllEvent: async (_, args, { datasources }, info) => {
    const { EventAccount } = datasources;
    const response = await new EventAccount().deleteAllEvent();
    return response;
  }
};

const EventQuery = {
  getEvents: async (_, args, { datasources, req }, info) => {
    if (!req.isAuth) {
      throw new Error("Unauthorized");
    }
    const { EventAccount } = datasources;
    const response = await new EventAccount().getEvents();
    return response;
  },

  getSingleEvent: async (_, { id }, { datasources }, info) => {
    const { EventAccount } = datasources;
    const response = await new EventAccount().getSingleEvent(id);
    return response;
  },

  getUserEvent: async (_, { userId }, { datasources }, info) => {
    const { EventAccount } = datasources;
    const response = await new EventAccount().getUserEvent(userId);
    return response;
  }
};

module.exports = { EventMutation, EventQuery };
