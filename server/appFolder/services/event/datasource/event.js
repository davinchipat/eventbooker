const Base = require("../../../base");
const Event = require("../../../model/event/event");
const User = require("../../../model/user/user");

class EventAccount extends Base {
  constructor() {
    super("Event Account");
  }

  //Mutation

  async createEvent(userId, data) {
    // this.isAuth;
    const user = await User.findById(userId);
    const newEvent = await new Event({ ...data, creator: user });
    newEvent.save();
    console.log(user);
    user.eventsCreated.push(newEvent);
    user.save();
    return "Event created";
  }

  async updateEvent(id, data) {
    const updated = await Event.findByIdAndUpdate(id, data);
    updated.save();
    return "event updated";
  }

  async deleteEvent(id) {
    const deleted = await Event.findByIdAndRemove(id);
    console.log(deleted);
    return "Deleted";
  }

  async deleteAllEvent() {
    // const allEvent = await Event.find()
    const allDeleted = await Event.deleteMany();
    console.log(allDeleted);
    return "all deleted";
  }

  //Query

  async getEvents() {
    const events = await Event.find().populate("creator");
    return events;
  }

  async getSingleEvent(id) {
    const event = await Event.findById(id);
    return event;
  }

  async getUserEvent(userId) {
    const user = await User.findById(userId);
    console.log(user);
    const userEvents = await Event.findById(user.id);
    return userEvents;
  }
}

module.exports = EventAccount;
