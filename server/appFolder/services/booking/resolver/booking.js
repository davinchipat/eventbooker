const bookingMutation = {
    bookEvent: async(_, {eventId}, {datasources}, info) => {
        const {BookingOperation} = datasources
        const response = await new BookingOperation().bookEvent(eventId)
        return response
    }
}

const bookingQuery = {

}

module.exports = {
    bookingMutation,
    bookingQuery
}