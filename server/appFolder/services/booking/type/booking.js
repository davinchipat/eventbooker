const {gql} = require('apollo-server-express')

module.exports = gql`
   extend type Query{
    bookings:[Booking]
   }

   extend type Mutation{
    bookEvent(eventId: ID ): Booking
    cancelBooking(id:ID): eventData
   }

   type Booking{
       _id: ID
       events: eventData
       user: userData
       createdAt: String
       updatedAt: String
   }
`