const Base = require("../../../base");
const Booking = require("../../../model/booking/booking");
const Event = require("../../../model/event/event");
const User = require("../../../model/user/user");

class BookingOperation extends Base {
  constructor() {
    super("Bookings");
  }

  //Mutation

  async bookEvent(eventId) {
    const eventToBook = await Event.findOne({ _id: eventId });
    console.log(eventToBook);
    const newBooking = await new Booking({
      user: "5d8511634b5008531b3a3e39",
      events: eventToBook
    });
    const result = await newBooking.save();
    console.log(result.createdAt);
    return result;
  }
}

module.exports = BookingOperation;
