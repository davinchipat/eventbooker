const EventAccount = require('./services/event/datasource/event')
const UserAccount = require('./services/user/datasource/user')
const BookingOperation = require('./services/booking/datasource/booking')

module.exports = {
    EventAccount,
    UserAccount,
    BookingOperation
}