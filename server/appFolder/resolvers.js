const {EventMutation, EventQuery} = require('./services/event/resolver/event')
const {userMutation,userQuery} = require('./services/user/resolver/user')
const {bookingQuery,bookingMutation} = require('./services/booking/resolver/booking')
const Query = {
    ...userQuery,
    ...EventQuery,
    ...bookingQuery
}


const Mutation = {
    ...userMutation,
    ...EventMutation,
    ...bookingMutation,
}

module.exports = {Query, Mutation}