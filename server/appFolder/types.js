const {gql} = require('apollo-server-express')
const  EventSchema = require('./services/event/type/event')
const UserSchema = require('./services/user/types/user')
const BookingSchema = require('./services/booking/type/booking')

const linkSchema = gql`
    scalar JSON
    scalar DateTime

    type Query {
        _:Boolean
    }

    enum gender {
        male
        female
    }

    type Mutation {
        _:Boolean
    }
`

module.exports = [linkSchema, EventSchema, UserSchema, BookingSchema]