const jwt = require( 'jsonwebtoken' );
const { SECRETKEY } = process.env

module.exports = async ( req, res, next ) => {
  try {
    let tokens = null;
    if ( req && req.cookies && req.cookies.token ) {
      tokens = req.cookies.token;
      req.user = await jwt.verify( tokens, SECRETKEY );
    }
    if ( req && req.headers['token'] ) {
      tokens = req.headers['token'];
      req.user = await jwt.verify( tokens, SECRETKEY );
    }
    return tokens;
  }
  catch ( e ) {
    if ( e.message.includes( 'expired' ) ) {
      req.user = { error: "Login expired" }
    }
  }
  finally {
    next()
  }
}