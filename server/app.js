const express = require("express");
const bodyParser = require("body-parser");
const { ApolloServer } = require("apollo-server-express");
const { typeDefs, resolvers } = require("./appFolder/schema");
const datasources = require("./appFolder/datasource");
const mongoose = require("mongoose");
const isAuth = require("./middleware/is-auth");
const DBURI = process.env.DBURI;
const app = express();
mongoose
  .connect("mongodb://localhost:27017/event_app", { useNewUrlParser: true })
  .then(res => {
    console.log("connected");
  })
  .catch(err => {
    console.log(`Error: ${err}`);
  });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(isAuth);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, res }) => ({
    req,
    res,
    datasources
  }),
  graphiql: true
});

server.applyMiddleware({ app, cors: false, path: "/" });

const port = process.env.PORT || 3001;

app.listen(port, () => {
  console.log(`app running on ${port}`);
});
